#!/usr/bin/env node

const { execSync } = require("child_process");
var path = require("path");

let options = {
  stdio: "inherit"
};

var express = require("express");
var app = express();
app.listen(3000, () => {
  console.log("Server running on port 3000");
});

app.get("/invoke", (req, res, next) => {
  res.send("done");
});

var middlewareAsyncExec = function(req, res, next) {
  console.log("Running Pagespeed on desktop ...");
  execSync(
    "lighthouse-batch -v -f sites.txt --html --out report/lighthouse/web --emulated-form-factor=desktop --params '--chrome-flags= --headless'",
    options
  );

  console.log("Running Pagespeed on mobile ...");
/*
  execSync(
    "lighthouse-batch -v -f sites.txt --html --out report/lighthouse/mobile --emulated-form-factor=mobile --params '--chrome-flags= --headless --disable-gpu --headless --window-size=412,732 --throttling-method=devtools'",
    options
  );*/

  console.log("Finished !!");

  
  next();
};

app.use(middlewareAsyncExec);


