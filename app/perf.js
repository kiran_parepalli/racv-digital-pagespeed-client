#!/usr/bin/env node

const { execSync } = require("child_process");
var path = require("path");

let options = {
  stdio: "inherit"
};

let today = new Date();
const dd = String(today.getDate()).padStart(2, '0');
const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
const yyyy = today.getFullYear();

let todayDate = dd + '-' + mm + '-' + yyyy;

const _runScript = function() {
  console.log("Running Pagespeed on desktop ...");
  execSync(
    "lighthouse https://www.racv.com.au/ --html --out report/lighthouse/"+todayDate+"/web --budget-path=budget.json  --emulated-form-factor=desktop --view",
    options
  );
  

  console.log("Running Pagespeed on mobile ...");
/*
  execSync(
    "lighthouse -v -f sites.txt --html --out report/lighthouse/"+todayDate+"/mobile --budget-path=budget.json  --params \"--chrome-flags= --headless --disable-gpu --headless --window-size=412,732 --emulated-form-factor=mobile \"",
    options
  );*/

  console.log("Finished !!");

};

_runScript();




