var path = require('path');
var express = require('express');
var app = express();

app.use('/', express.static('report'));


var server = app.listen(3033, function () {
    var host = 'localhost';
    var port = server.address().port;
    console.log('listening on http://'+host+':'+port+'/');
});